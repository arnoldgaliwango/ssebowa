
        
$(document).ready(function(){
  $('.sidenav').sidenav();
  $('.materialboxed').materialbox();
  $('.parallax').parallax();
  $('.tabs').tabs();
  $('.datepicker').datepicker({
      disableWeekends: true
  });
  $('.tooltipped').tooltip();
  $('.scrollspy').scrollSpy();
  $('.collapsible').collapsible({
    // accordion: false
  });
  $('.modal').modal();

  $('.about .collapsible-header').click(function(){
    $('.about i.material-icons').toggleClass("spinAbout");
    // $('.about i.material-icons').removeClass("spinIcon");
  });
  $('.collapsible-header').click(function(){
    $(this).find(':first').toggleClass('spinIcon');
    //$(this).toggleClass("spinIcon");
  });

  $("input").change(function() {
    if($(this).is(":checked")) {
      $('.collapsible-body').addClass("expandAll");
      // $('i.material-icons').toggleClass('spinIcon');
    }
    else {
      $('.collapsible-body').removeClass("expandAll");
    }
  });

  $('.single-announce-heading').click(function(){
    $('.announcement-body').toggleClass('hide-announcement');
    //$(this).toggleClass("spinIcon");
  });

  

  $('.menu-icon').click(function(){
    $('.drawer').toggleClass('open-drawer');
    //$(this).toggleClass("spinIcon");
  });
  $('.wrapper').click(function(){
    $('.drawer').removeClass('open-drawer');
    //$(this).toggleClass("spinIcon");
  });

  $("#_header").load("_header.html"); 
  
});


// Scroll Up to Hide Header

(function(){

  var doc = document.documentElement;
  var w = window;

  var prevScroll = w.scrollY || doc.scrollTop;
  var curScroll;
  var direction = 0;
  var prevDirection = 0;

  var header = document.getElementById('site-header');
  var footer = document.getElementById('site-footer');

  var checkScroll = function() {

  /*
  ** Find the direction of scroll
  ** 0 - initial, 1 - up, 2 - down
  */

  curScroll = w.scrollY || doc.scrollTop;
  if (curScroll > prevScroll) { 
    //scrolled up
    direction = 2;
  }
  else if (curScroll < prevScroll) { 
    //scrolled down
    direction = 1;
  }

  if (direction !== prevDirection) {
    toggleHeader(direction, curScroll);
  }
  
  prevScroll = curScroll;
};

var toggleHeader = function(direction, curScroll) {
  if (direction === 2 && curScroll > 52) { 
    
    //replace 52 with the height of your header in px

    header.classList.add('hide-header');
    footer.classList.add('hide-footer');
    prevDirection = direction;
  }
  else if (direction === 1) {
    header.classList.remove('hide-header');
    footer.classList.remove('hide-footer');
    prevDirection = direction;
  }
};

window.addEventListener('scroll', checkScroll);

})();


//Get the button
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}

$(".sidebar-images").slick({
  // dots: true,
  // infinite: true,
  // slidesToShow: 1,
  // slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 4000,
  // arrows: false
  dots: true,
  vertical: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  autoplay: true,
  autoplaySpeed: 4000,
  infinite: true,
});
